
//
//  ViewController.swift
//  Hostile Territory
//
//  Created by James Kirwan on 7/2/18.
//

import Cocoa
import SpriteKit
import GameplayKit

class ViewController: NSViewController {

    var skView: SKView!
    
    var scene: MenuScene!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let view = self.skView!
        
        scene = MenuScene(size: view.bounds.size)
        scene.scaleMode = .resizeFill
        
        view.setFrameSize(NSSize(width: 1440, height: 900))
        view.presentScene(scene)

    }
}

