//
//  Player.swift
//  Hostile Territory
//
//  Created by James Kirwan on 11/5/18.
//

import Foundation
import SpriteKit

class Player:SKSpriteNode {

    private let playerCategory:UInt32 = 0x1 << 0
    private let boxCategory:UInt32 = 0x1 << 1
    private let bulletCategory:UInt32 = 0x1 << 2
    private let pUpCategory:UInt32 = 0x1 << 3
    
    var bombArray:[Bomb] = []
    
    internal var moveSpeed:Double = 5
    internal var bombRadius = 320
    internal var ammo = 1

    internal var health:CGFloat = 100.0
    internal var paint:CGFloat = 100.0
    internal var moveLvL:Int = 1
    internal var radiusLvL:Int = 1
    internal var ammoLvL:Int = 1
    
    internal var painted:Int = 0
    internal var kills:Int = 0
    internal var deaths:Int = 0
    
    internal var playerColor:String!
    
    internal let barrel = SKSpriteNode(imageNamed: "bBlue")
    
    init(PlayerNumber: Int) {
        
        var tankText = SKTexture(imageNamed: "tBlue")
        var barrel = SKSpriteNode(imageNamed: "bBlue")
        playerColor = "Blue"
        
        if(PlayerNumber == 2) {
            tankText = SKTexture(imageNamed: "tRed")
            barrel = SKSpriteNode(imageNamed: "bRed")
            playerColor = "Red"
        }
        
        super.init(texture: tankText, color: .clear, size: CGSize(width: 83 , height: 78))
        
        name = "player"
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(4.0)
        
        physicsBody = SKPhysicsBody(texture: tankText, size: tankText.size())
        physicsBody?.affectedByGravity = false
        
        physicsBody?.categoryBitMask = playerCategory
        physicsBody?.contactTestBitMask = bulletCategory | boxCategory | pUpCategory
        physicsBody?.usesPreciseCollisionDetection = false
        
        barrel.anchorPoint = CGPoint(x: 0.5, y: 0)
        self.addChild(barrel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
}
