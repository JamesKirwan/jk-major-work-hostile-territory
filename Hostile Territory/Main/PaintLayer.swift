//
//  PaintLayer.swift
//  Hostile Territory
//
//  Created by James Kirwan on 10/5/18.
//

import Foundation
import SpriteKit

class PaintLayer:SKTileMapNode {
        
    override init() {
        
        let tileSet = SKTileSet(named: "colorSet")!
        
        super.init(tileSet: tileSet, columns: 752, rows: 480, tileSize: CGSize(width: 4, height: 4))
        
        name = "PaintLayer"
        position = CGPoint(x: 0, y: 0)
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(2.0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fillPaintLayerTile(Position: CGPoint, Player: Player) {

        let col = self.tileColumnIndex(fromPosition: Position)
        let row = self.tileRowIndex(fromPosition: Position)
        var tile = self.tileSet.tileGroups[0]
        
        if (Player.playerColor == "Red") {
            tile = self.tileSet.tileGroups[1]
        }
        
        if(self.tileGroup(atColumn: col, row: row) != tile) {
            self.setTileGroup(tile, forColumn: col, row: row)
            Player.painted += 1
        }
        
    }

}
