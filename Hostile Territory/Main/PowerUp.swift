//
//  PowerUp.swift
//  Hostile Territory
//
//  Created by James Kirwan on 14/5/18.
//

import Foundation
import SpriteKit

class PowerUp:SKSpriteNode {
    
    private let playerCategory:UInt32 = 0x1 << 0
    private let boxCategory:UInt32 = 0x1 << 1
    private let bulletCategory:UInt32 = 0x1 << 2
    private let pUpCategory:UInt32 = 0x1 << 3
    
    var powerUpTypes:[String] = ["speed","radius","ammo"]
    var type:String!
    
    init() {
        
        super.init(texture: nil, color: .clear, size: CGSize(width: 64, height: 64))
        texture = selectType()
        
        name = "PowerUp"
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(3.0)
        
        physicsBody = SKPhysicsBody(texture: texture!, size: size)
        physicsBody?.affectedByGravity = false
        physicsBody?.pinned = true
        physicsBody?.allowsRotation = false
        
        physicsBody?.categoryBitMask = pUpCategory
        physicsBody?.contactTestBitMask = playerCategory | bulletCategory | boxCategory
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func selectType() -> SKTexture {
        
        let random = arc4random_uniform(UInt32(powerUpTypes.count))
        
        type = powerUpTypes[Int(random)]
        
        let text:SKTexture!
        
        switch (type) {
        case "speed":
            text = SKTexture(imageNamed: "speed")
        case "radius":
            text = SKTexture(imageNamed: "radius")
        case "ammo":
            text = SKTexture(imageNamed: "ammo")
        default:
            text = SKTexture(imageNamed: "Speed")
        }
        
        return text
        
    }
    
    func effect(Player: Player) {
        
        switch type {
        case "speed":
            if(Player.moveSpeed < 8.0) {
                Player.moveSpeed += 0.6
                Player.moveLvL += 1
                self.removeFromParent()
            }
        case "radius":
            if(Player.bombRadius < 620) {
                Player.bombRadius += 64
                Player.radiusLvL += 1
                self.removeFromParent()
            }
        case "ammo":
            if(Player.ammo < 5) {
                Player.ammo += 1
                Player.ammoLvL += 1
                self.removeFromParent()
            }
        default:
            break
        }
    
    }
    
}
