//
//  Box.swift
//  Hostile Territory
//
//  Created by James Kirwan on 18/6/18.
//

import Foundation
import SpriteKit

class Box:SKSpriteNode {
    
    private let playerCategory:UInt32 = 0x1 << 0
    private let boxCategory:UInt32 = 0x1 << 1
    private let bulletCategory:UInt32 = 0x1 << 2
    private let pUpCategory:UInt32 = 0x1 << 3
    
    init() {
        
        let boxText = SKTexture(imageNamed: "box")
        
        super.init(texture: boxText, color: .clear, size: boxText.size())
        
        name = "Box"
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(3.0)
        
        physicsBody = SKPhysicsBody(rectangleOf: size)
        physicsBody?.affectedByGravity = false
        physicsBody?.pinned = true
        physicsBody?.allowsRotation = false
        physicsBody?.restitution = CGFloat(0)
        
        physicsBody?.categoryBitMask = boxCategory
        physicsBody?.contactTestBitMask = playerCategory | bulletCategory | pUpCategory

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
