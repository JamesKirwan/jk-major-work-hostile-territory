//
//  Map.swift
//  Hostile Territory
//
//  Created by James Kirwan on 9/5/18.
//

import Foundation
import SpriteKit

class MapLayer:SKTileMapNode {
    
    override init() {
        
        let tileSet = SKTileSet(named: "backgroundSet")!
        
        super.init(tileSet: tileSet, columns: 47, rows: 30, tileSize: CGSize(width: 64, height: 64))
        
        name = "MapLayer"
        position = CGPoint(x: 0, y: 0)
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(1)
        
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        physicsBody?.affectedByGravity = false
        
        self.fill(with: tileSet.tileGroups[0])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
