//
//  GameScene.swift
//  Hostile Territory
//
//  Created by James Kirwan on 7/2/18.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // Initialises Objects
    var mapLayer:MapLayer!
    var paintLayer:PaintLayer!
    var objectLayer:ObjectLayer!
    var barrel:SKSpriteNode!
    var player1:Player!
    var player2:Player!
    var gui:GUI!
    
    // Array Tracking Keystrokes
    var keysPressed:[UInt16] = []
    
    // Time
    var systemTime:Double!
    var elapsed:Double = 0
    var p1RTime:Double = 0
    var p2RTime:Double = 0
    var regenTime:Double = 0
    
    // Collision Categories
    private let playerCategory:UInt32 = 0x1 << 0
    private let otherCategory:UInt32 = 0x1 << 1
    
    // Function called when the game loads
    override func didMove(to view: SKView) {
        
        run(SKAction.playSoundFileNamed("music.wav", waitForCompletion: false))
        
        // Starts the Physics Simulator
        self.physicsWorld.contactDelegate = self
        
        // Creating and Loading the Camera
        let camera:SKCameraNode = SKCameraNode()
        camera.setScale(CGFloat(2.85))
        camera.zPosition = CGFloat(6.0)
        self.camera = camera
        self.addChild(camera)

        let background = SKSpriteNode(imageNamed: "background")
        background.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background.position = CGPoint(x: 0, y: 0)
        background.zPosition = CGFloat(1.0)
        self.addChild(background)
        
        // Loads Layers and Player
        loadLayers()
        loadPlayers()
        
        // Loads all the GUI
        gui = GUI()
        gui.setCamera(Camera: self.camera!)
        gui.loadGameGUI()
        gui.loadp1GUI()
        gui.loadp2GUI()
        
    }
    
    private func loadLayers() {
        
        // Creating and Loading the MapLayer
        mapLayer = MapLayer()
        self.addChild(mapLayer)
        
        // Creating and Loading the PaintLayer
        paintLayer = PaintLayer()
        self.addChild(paintLayer)
        
        // Creating and Loading the ObjectLayer
        objectLayer = ObjectLayer()
        self.addChild(objectLayer)
        
        // Creating and Loading the Map Objects
        for x in 0...objectLayer.numberOfColumns-1 {
            for y in 0...objectLayer.numberOfRows-1 {
                let rnd = arc4random_uniform(UInt32(5))
                if rnd == 0 {
                    let box:Box = Box()
                    box.position = objectLayer.centerOfTile(atColumn: x, row: y)
                    self.addChild(box)
                }
            }
        }
        
    }
    
    private func loadPlayers() {
        
        // Creating Player1, Setting it's position and Loading it to the Scene
        player1 = Player(PlayerNumber: 1)
        player1.position = CGPoint(x: -(self.frame.width / 2), y: 0)
        self.addChild(player1)
        
        // Creating Player2, Setting it's position and Loading it to the Scene
        player2 = Player(PlayerNumber: 2)
        player2.position = CGPoint(x: (self.frame.width / 2), y: 0)
        self.addChild(player2)
        
    }
    
    override func keyDown(with event: NSEvent) {
        
        // Tracking all Key Presses
        if(!keysPressed.contains(event.keyCode)) {
            keysPressed.append(event.keyCode)
        }
        
        if(event.keyCode == 7) {
            spawnBomb(player: player1)
        }
        
        if(event.keyCode == 46) {
            spawnBomb(player: player2)
        }
        
    }
    
    override func keyUp(with event: NSEvent) {
        
        // Removing Key Presses
        let index = keysPressed.index(of: event.keyCode)
        keysPressed.remove(at: index!)
        
    }
    
    func player1Controls() {
        
        if(keysPressed.count != 0) {
            
            let player:Player = player1
            let moveSpeed:CGFloat = CGFloat(player.moveSpeed)
            
            if(keysPressed.contains(13)) {
                player.position = CGPoint(x: player.position.x - (moveSpeed * sin(player.zRotation)), y: player.position.y + (moveSpeed * cos(player.zRotation)))
                paintTrail(Player: player)
            }
            
            if(keysPressed.contains(1)) {
                player.position = CGPoint(x: player.position.x + (moveSpeed * sin(player.zRotation)), y: player.position.y - (moveSpeed * cos(player.zRotation)))
                paintTrail(Player: player)
            }
            
            if(keysPressed.contains(2)) {
                player.zRotation = player.zRotation - 0.05
            }
            
            if(keysPressed.contains(0)) {
                player.zRotation = player.zRotation + 0.05
            }
            if(keysPressed.contains(6) && p1RTime >= 1.0) {
                spawnBullet(player: player)
                p1RTime = 0
            }

        }
        
    }
    
    func player2Controls() {
        
        if(keysPressed.count != 0) {
            
            let player:Player = player2
            let moveSpeed:CGFloat = CGFloat(player.moveSpeed)
            
            if(keysPressed.contains(32)) {
                player.position = CGPoint(x: player.position.x - (moveSpeed * sin(player.zRotation)), y: player.position.y + (moveSpeed * cos(player.zRotation)))
                paintTrail(Player: player)
            }
            
            if(keysPressed.contains(38)) {
                player.position = CGPoint(x: player.position.x + (moveSpeed * sin(player.zRotation)), y: player.position.y - (moveSpeed * cos(player.zRotation)))
                paintTrail(Player: player)
            }
            
            if(keysPressed.contains(40)) {
                player.zRotation = player.zRotation - 0.05
            }
            
            if(keysPressed.contains(4)) {
                player.zRotation = player.zRotation + 0.05
            }
            if(keysPressed.contains(45) && p2RTime >= 1.0) {
                spawnBullet(player: player)
                p2RTime = 0
            }
            
        }
        
    }
    
    func respawn(player: Player) {
        
        run(SKAction.playSoundFileNamed("death.wav", waitForCompletion: false))
        
        player.health = 100.0
        player.paint = 100.0
        player.deaths += 1
        player.zRotation = CGFloat(0)
        
        if(player.playerColor == "Red") {
            player.position = CGPoint(x: (self.frame.width / 2), y: 0)
            player1.kills += 1
            gui.createKillFeedObject(Player: "Player 1", Killed: "Player 2")
        }
        else {
            player.position = CGPoint(x: -(self.frame.width / 2), y: 0)
            player2.kills += 1
            gui.createKillFeedObject(Player: "Player 2", Killed: "Player 1")
        }
        
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        var player:Player! = nil
        var box:Box! = nil
        var bullet:Bullet! = nil
        var powerUp:PowerUp! = nil
        
        switch contact.bodyA.node?.name {
        case "player":
            player = contact.bodyA.node as! Player
        case "Bullet":
            bullet = contact.bodyA.node as! Bullet
        case "Box":
            box = contact.bodyA.node as! Box
        case "PowerUp":
            powerUp = contact.bodyA.node as! PowerUp
        default:
            break
        }
        
        switch contact.bodyB.node?.name {
        case "player":
            player = contact.bodyB.node as! Player
        case "Bullet":
            bullet = contact.bodyB.node as! Bullet
        case "Box":
            box = contact.bodyB.node as! Box
        case "PowerUp":
            powerUp = contact.bodyB.node as! PowerUp
        default:
            break
        }
        
        if ((player != nil) && (bullet != nil)) {
            
            // Determines Player
            if(player.playerColor == "Blue") {
                player = player1
            }
            else {
                player = player2
            }
            
            // Removes Player
            if(player.health - 25 <= 0) {
                player.health -= 25
            }
            else {
                player.health -= 25
            }
            
            // Removes Bullet
            bullet.removeFromParent()
            
            // Paint Splatter
            for x in (-16)...(16) {
                for y in (-16)...(16) {
                    if (x*x + y*y) <= (16)*(16) {
                        paintLayer.fillPaintLayerTile(Position: CGPoint(x: Int(bullet.position.x) + x, y: Int(bullet.position.y) + y), Player: player)
                    }
                }
            }
            
        }
        
        if ((box != nil) && (bullet != nil)) {
                        
            // Removes Bullet
            bullet.removeFromParent()
            
            // Paint Splatter
            for x in (-16)...(16) {
                for y in (-16)...(16) {
                    if (x*x + y*y) <= (16)*(16) {
                        paintLayer.fillPaintLayerTile(Position: CGPoint(x: Int(bullet.position.x) + x, y: Int(bullet.position.y) + y), Player: bullet.player)
                    }
                }
            }
            
            // Spawns Power Up
            let rnd = arc4random_uniform(UInt32(4))
            if rnd == 0 {
                spawnPowerUp(Position: objectLayer.getCenterOfTile(Position: box.position))
            }
            
            box.removeFromParent()
        
        }
        
        if ((bullet != nil) && (player == nil && box == nil && powerUp == nil)) {
            bullet.removeFromParent()
        }
        
        if ((player != nil) && (powerUp != nil)) {
            
            if(player.playerColor == "Blue") {
                player = player1
            }
            else {
                player = player2
            }
            
            powerUp.removeFromParent()
            powerUp.effect(Player: player)
            
        }
        
    }
    
    func paintTrail(Player: Player) {
        
        // Paints a trail behind the Player
        let startPos = CGPoint(x: Player.position.x - (Player.size.width / 2), y: Player.position.y + (Player.size.height / 2))
        let endPos = CGPoint(x: startPos.x + Player.size.width, y: startPos.y + Player.size.width)
        let deltaX:Int = Int(endPos.x - startPos.x)
        let deltaY:Int = Int(endPos.y - startPos.y)
        
        for x in 0...(deltaX-1)/8 {
            for y in 0...(deltaY-1)/8 {
                paintLayer.fillPaintLayerTile(Position: CGPoint(x: Int(startPos.x) + (x * 8), y: Int(startPos.y) - Int(Player.size.height) + (y * 8)), Player: Player)
            }
        }
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        if (gui.gameTime == 0) {
            let scene:MenuScene = MenuScene(size: self.size)
            self.view?.presentScene(scene)
        }
        
        // Sets the Inital System Time
        if(systemTime == nil) {
            systemTime = currentTime
        }
        
        // Set the other Time Variables
        elapsed = (currentTime - systemTime)
        p1RTime += elapsed
        p2RTime += elapsed
        regenTime += elapsed
        systemTime = currentTime
        
        if (regenTime >= 1.0) {
            
            regenTime = 0
            
            if(player1.paint + 2 < 100) {
                player1.paint += 2
            }
            if(player1.health + 2 < 100) {
                player1.health += 2
            }
            if(player2.paint + 2 < 100) {
                player2.paint += 2
            }
            if(player2.health + 2 < 100) {
                player2.health += 2
            }
        }
        
        if (player1.health <= 0) {
            respawn(player: player1)
        }
        if (player2.health <= 0) {
            respawn(player: player2)
        }
        
        // Moves the Bullets
        moveBullet()
        
        // Player 1 Controls
        player1Controls()
        
        // Player 2 Controls
        player2Controls()
        
        // Update the Games GUI
        gui.UPGameGUI(player1: player1, player2: player2)
        gui.UPp1GUI(Player: player1)
        gui.UPp2GUI(Player: player2)
                
    }
    
    private func moveBullet() {
        
        // Moves Every Bullet
        for child in self.children {
            if child.name == "Bullet" {
                let bullet:SKSpriteNode = child as! SKSpriteNode
                bullet.position = CGPoint(x: bullet.position.x - (15.0 * sin(bullet.zRotation)), y: bullet.position.y + (15.0 * cos(bullet.zRotation)))
            }
            
        }
        
    }
    
    private func spawnBullet(player: Player) {
        
        // Creates the Bullet
        
        if (player.paint - 10.0 >= 0) {
        
            player.paint -= 10.0
            
            let bullet:Bullet = Bullet(Player: player)
            bullet.position = CGPoint(x: player.position.x - (player.barrel.size.height * sin(player.zRotation)), y: player.position.y + (player.barrel.size.height * cos(player.zRotation)))
            bullet.zRotation = player.zRotation
            
            bullet.color = player.color
            
            // Loads the Bullet
            self.addChild(bullet)
            
            //Starts Despawn Timer
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.despawnBullet), userInfo: ["bullet" : bullet, "player": player], repeats: false)
            
            run(SKAction.playSoundFileNamed("bullet.wav", waitForCompletion: false))
        }
    }
    
    @objc func despawnBullet(timer: Timer) {
        
        let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
        let bullet:Bullet = (userInfo["bullet"] as! Bullet)
        let player:Player = (userInfo["player"] as! Player)
        
        // Removes Bullet
        bullet.removeFromParent()
        
        // Paint Splatter
        for x in (-16)...(16) {
            for y in (-16)...(16) {
                if (x*x + y*y) <= (16)*(16) {
                    paintLayer.fillPaintLayerTile(Position: CGPoint(x: Int(bullet.position.x) + x, y: Int(bullet.position.y) + y), Player: player)
                }
            }
        }
        
    }
    
    private func spawnBomb(player: Player) {
        
        // Checks if the Player has Ammo
        
        if ((player.bombArray.count < player.ammo)) {
            
            // Creates Bomb
            let bomb:Bomb = Bomb()
            bomb.position = objectLayer.getCenterOfTile(Position: player.position)
            bomb.Radius = player.bombRadius
            
            // Loads Bullet
            self.addChild(bomb)
            player.bombArray.append(bomb)
            
            // Starts Detonation Timer
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.detonateBomb), userInfo: ["bomb" : bomb, "player": player], repeats: false)
            
        }
        
    }
    
    private func findDistance(p1: CGPoint, p2: CGPoint) -> CGFloat {
        return sqrt(pow(p1.x - p2.x,2) + pow(p1.y - p2.y,2))
    }
    
    @objc func detonateBomb(timer: Timer) {
        
        let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
        let bomb:Bomb = (userInfo["bomb"] as! Bomb)
        let player:Player = (userInfo["player"] as! Player)
        
        // Paint Splatter
        for x in (-bomb.Radius/2)...(bomb.Radius/2) {
            for y in (-bomb.Radius/2)...(bomb.Radius/2) {
                if (x*x + y*y) <= (bomb.Radius/2)*(bomb.Radius/2) {
                    paintLayer.fillPaintLayerTile(Position: CGPoint(x: Int(bomb.position.x) + x, y: Int(bomb.position.y) + y), Player: player)
                }
            }
        }
        
        for child in self.children {
            if (child.name == "Box") {
                if (findDistance(p1: bomb.position, p2: child.position) <= CGFloat(player.bombRadius/2 + 10)) {
                    breakBox(Box: (child as! Box))
                }
            }
        }
        
        if (findDistance(p1: bomb.position, p2: player1.position) <= CGFloat(player.bombRadius/2 + 10)) {
            player1.health -= 50
        }
        if (findDistance(p1: bomb.position, p2: player2.position) <= CGFloat(player.bombRadius/2 + 10)) {
            player2.health -= 50
        }
        
        // Removes Bomb
        bomb.removeFromParent()
        player.bombArray.remove(at: player.bombArray.index(of: bomb)!)
        
        run(SKAction.playSoundFileNamed("bomb.wav", waitForCompletion: false))
        
    }
    
    private func spawnPowerUp(Position: CGPoint) {
        
        // Creates Power Up
        let powerUp:PowerUp = PowerUp()
        powerUp.position = Position
        self.addChild(powerUp)
        
        // Starts Despawn PowerUp
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.despawnPowerUp), userInfo: ["powerUp" : powerUp], repeats: false)
        
    }
    
    @objc func despawnPowerUp(timer: Timer) {
        
        let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
        let powerUp:PowerUp = (userInfo["powerUp"] as! PowerUp)
        
        // Removes Power Up
        powerUp.removeFromParent()
        
    }

    @objc func breakBox(Box: Box) {
        
        // Spawns Power Up
        let rnd = arc4random_uniform(UInt32(4))
        if rnd == 0 {
            spawnPowerUp(Position: objectLayer.getCenterOfTile(Position: Box.position))
        }

        Box.removeFromParent()
    }
    
}
