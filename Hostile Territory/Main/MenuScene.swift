//
//  MenuScene.swift
//  Hostile Territory
//
//  Created by James Kirwan on 25/5/18.
//

import Foundation
import SpriteKit

class MenuScene: SKScene {
    
    var button:SKSpriteNode!
    
    override func didMove(to view: SKView) {
        
        let camera:SKCameraNode = SKCameraNode()
        self.camera = camera
        self.addChild(camera)
        
        let background:SKSpriteNode = SKSpriteNode(imageNamed: "background-menu")
        background.position = CGPoint(x: 0, y: 0)
        background.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.addChild(background)
        
        let title:SKSpriteNode = SKSpriteNode(imageNamed: "title")
        title.position = CGPoint(x: 0, y: 300)
        title.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.addChild(title)
        
        button = SKSpriteNode(color: .darkGray, size: CGSize(width: 200, height: 75))
        button.name = "button"
        button.position = CGPoint(x: 0, y: 0)
        button.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.addChild(button)
        
        let bLab:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        bLab.text = "Start"
        bLab.fontColor = .white
        bLab.fontSize = 64
        bLab.position = CGPoint(x:0, y: -15)
        self.addChild(bLab)
        
        let infop = SKSpriteNode(color: .darkGray, size: CGSize(width: 250, height: 150))
        infop.name = "infop"
        infop.position = CGPoint(x: 0, y: -210)
        infop.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.addChild(infop)
        
        let info:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info.text = "Controls"
        info.fontColor = .white
        info.fontSize = 32
        info.position = CGPoint(x:0, y: -160)
        self.addChild(info)
        
        let info1:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info1.text = "Forward: W/U"
        info1.fontColor = .white
        info1.fontSize = 32
        info1.position = CGPoint(x:0, y: -180)
        self.addChild(info1)
        
        let info2:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info2.text = "Backwards: S/J"
        info2.fontColor = .white
        info2.fontSize = 32
        info2.position = CGPoint(x:0, y: -200)
        self.addChild(info2)
        
        let info3:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info3.text = "Left: A/H"
        info3.fontColor = .white
        info3.fontSize = 32
        info3.position = CGPoint(x:0, y: -220)
        self.addChild(info3)
        
        let info4:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info4.text = "Right: D/K"
        info4.fontColor = .white
        info4.fontSize = 32
        info4.position = CGPoint(x:0, y: -240)
        self.addChild(info4)
        
        let info5:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info5.text = "Shoot: Z/N"
        info5.fontColor = .white
        info5.fontSize = 32
        info5.position = CGPoint(x:0, y: -260)
        self.addChild(info5)
        
        let info6:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        info6.text = "Bomb: X/M"
        info6.fontColor = .white
        info6.fontSize = 32
        info6.position = CGPoint(x:0, y: -280)
        self.addChild(info6)
        
    }
    
    override func mouseDown(with event: NSEvent) {
        if(button.frame.contains(event.location(in: self))) {
            let scene:GameScene = GameScene(size: self.size)
            self.view?.presentScene(scene)
        }
    }
    
}
