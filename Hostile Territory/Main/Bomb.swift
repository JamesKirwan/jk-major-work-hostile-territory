//
//  Bomb.swift
//  Hostile Territory
//
//  Created by James Kirwan on 14/5/18.
//

import Foundation
import SpriteKit

class Bomb:SKSpriteNode {
        
    var Radius:Int!
    
    init() {
        
        let bombText = SKTexture(imageNamed: "bomb")
        
        super.init(texture: bombText, color: .clear, size: bombText.size())
        
        name = "Bomb"
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(3.0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

