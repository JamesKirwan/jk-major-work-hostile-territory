//
//  ObjectLayer.swift
//  Hostile Territory
//
//  Created by James Kirwan on 18/6/18.
//

import Foundation
import SpriteKit

class ObjectLayer:SKTileMapNode {
    
    override init() {
        
        let tileSet = SKTileSet(named: "colorSet")!
        super.init(tileSet: tileSet, columns: 33, rows: 21, tileSize: CGSize(width: 90, height: 90))
        
        name = "ObjectLayer"
        position = CGPoint(x: 0, y: 0)
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(3.0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getCenterOfTile(Position: CGPoint) -> CGPoint {
        
        let col = self.tileColumnIndex(fromPosition: Position)
        let row = self.tileRowIndex(fromPosition: Position)
        
        return self.centerOfTile(atColumn: col, row: row)
    }
    
}
