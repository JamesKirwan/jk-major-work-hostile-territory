//
//  GUI.swift
//  Hostile Territory
//
//  Created by James Kirwan on 26/7/18.
//

import Foundation
import SpriteKit

class GUI {
    
    internal var camera:SKCameraNode! = nil
    
    // Player 1 Section 1
    
    private var p1h:SKSpriteNode = SKSpriteNode(color: .red, size: CGSize(width: 64, height: 128))
    private var p1hl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    private var p1p:SKSpriteNode = SKSpriteNode(color: .green, size: CGSize(width: 64, height: 128))
    private var p1pl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Player 1 Section 2
    
    private var p1a:SKSpriteNode = SKSpriteNode(imageNamed: "ammo")
    private var p1al:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    private var p1r:SKSpriteNode = SKSpriteNode(imageNamed: "radius")
    private var p1rl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    private var p1s:SKSpriteNode = SKSpriteNode(imageNamed: "speed")
    private var p1sl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Player 1 Section 3
    
    private var p1kl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    private var p1dl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    private var p1ptl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Player 2 Section 1
    
    private var p2h:SKSpriteNode = SKSpriteNode(color: .red, size: CGSize(width: 64, height: 128))
    private var p2hl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    private var p2p:SKSpriteNode = SKSpriteNode(color: .green, size: CGSize(width: 64, height: 128))
    private var p2pl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Player 2 Section 2
    
    private var p2a:SKSpriteNode = SKSpriteNode(imageNamed: "ammo")
    private var p2al:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    private var p2r:SKSpriteNode = SKSpriteNode(imageNamed: "radius")
    private var p2rl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    private var p2s:SKSpriteNode = SKSpriteNode(imageNamed: "speed")
    private var p2sl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Player 2 Section 3
    
    private var p2kl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    private var p2dl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    private var p2ptl:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Game UI
    
    private var team1Label:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    private var team2Label:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    private var timeLabel:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
    
    // Game Time
    
    var gameTime = 300
    
    // Set the Camera
    
    public func setCamera(Camera: SKCameraNode) {
        
        camera = Camera
        
    }
    
    // Load Game GUI
    
    func loadGameGUI() {
        
        let title = SKSpriteNode(imageNamed: "title")
        title.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        title.position = CGPoint(x: 0, y: -390)
        camera.addChild(title)
        
        team1Label.fontColor = .white
        team1Label.fontSize = 48
        team1Label.position = CGPoint(x: -75, y: 370)
        team1Label.text = "Team1: 0%"
        team1Label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        camera.addChild(team1Label)
        
        team2Label.fontColor = .white
        team2Label.fontSize = 48
        team2Label.position = CGPoint(x: 75, y: 370)
        team2Label.text = "Team2: 0%"
        team2Label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        camera.addChild(team2Label)
        
        timeLabel.fontColor = .white
        timeLabel.fontSize = 48
        timeLabel.position = CGPoint(x: 0, y: 370)
        timeLabel.text = String(gameTime)
        camera.addChild(timeLabel)
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timeTick), userInfo: nil, repeats: true)
        
    }
    
    // Update Game GUI
    
    func UPGameGUI(player1: Player, player2: Player) {
        
        let total = player1.painted + player2.painted
        
        if (total > 0) {
        
            let team1 = round((Double(player1.painted) / Double(total)) * 100)
            team1Label.text = "Team1: " + String(Int(team1)) + "%"
            
            let team2 = round((Double(player2.painted) / Double(total)) * 100)
            team2Label.text = "Team2: " + String(Int(team2)) + "%"
        
        }
        
    }
    
    // Load Player 1 GUI
    
    func loadp1GUI() {
        
        let pannel:SKSpriteNode = SKSpriteNode(imageNamed: "pannel")
        pannel.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        pannel.position = CGPoint(x: -625, y: 0)
        camera.addChild(pannel)
        
        let playerLabel:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        playerLabel.fontColor = .white
        playerLabel.fontSize = 32
        playerLabel.text = "Player 1"
        playerLabel.position = CGPoint(x: -625, y: 290)
        camera.addChild(playerLabel)
        
        // Section 1
        p1GUISec1()
        
        // Section 2
        p1GUISec2()
        
        // Section 3
        p1GUISec3()
        
    }
    
    // Update Player 1 GUI
    
    func UPp1GUI(Player: Player) {
        
        UPp1Sec1(Player: Player)
        UPp1Sec2(Player: Player)
        UPp1Sec3(Player: Player)

    }
    
    // Load Player 2 GUI
    
    func loadp2GUI() {
        
        let pannel:SKSpriteNode = SKSpriteNode(imageNamed: "pannel")
        pannel.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        pannel.position = CGPoint(x: 625, y: 0)
        camera.addChild(pannel)
        
        let playerLabel:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        playerLabel.fontColor = .white
        playerLabel.fontSize = 32
        playerLabel.text = "Player 2"
        playerLabel.position = CGPoint(x: 625, y: 290)
        camera.addChild(playerLabel)
        
        p2GUISec1()
        
        p2GUISec2()
        
        p2GUISec3()
    
    }
    
    // Update Player 2 GUI
    
    func UPp2GUI(Player: Player) {
        
        UPp2Sec1(Player: Player)
        UPp2Sec2(Player: Player)
        UPp2Sec3(Player: Player)
        
    }
    
    // Player 1 Section 1
    
    private func p1GUISec1() {
        
        let health = p1h
        let healthLabel = p1hl
        
        let paint = p1p
        let paintLabel = p1pl
        
        health.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        health.position = CGPoint(x: -665, y: 200)
        camera.addChild(health)
        
        healthLabel.fontColor = .white
        healthLabel.fontSize = 16
        healthLabel.text = "Health"
        healthLabel.position = CGPoint(x: -665, y: 115)
        camera.addChild(healthLabel)
        
        paint.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        paint.position = CGPoint(x: -585, y: 200)
        camera.addChild(paint)
        
        paintLabel.fontColor = .white
        paintLabel.fontSize = 16
        paintLabel.text = "Paint"
        paintLabel.position = CGPoint(x: -585, y: 115)
        camera.addChild(paintLabel)
        
    }
    
    // Update Player 1 Section 1
    
    private func UPp1Sec1(Player: Player) {
        
        p1h.position.y -= (p1h.size.height - CGFloat((Player.health / 100.0) * 125.0)) / 2
        p1h.size.height = CGFloat((Player.health / 100.0) * 125.0)
        
        p1p.position.y -= (p1p.size.height - CGFloat((Player.paint / 100.0) * 125.0)) / 2
        p1p.size.height = CGFloat((Player.paint / 100.0) * 125.0)
        
    }
    
    // Player 2 Section 1
    
    private func p2GUISec1() {
        
        let health = p2h
        let healthLabel = p2hl
        
        let paint = p2p
        let paintLabel = p2pl
        
        health.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        health.position = CGPoint(x: 585, y: 200)
        camera.addChild(health)
        
        healthLabel.fontColor = .white
        healthLabel.fontSize = 16
        healthLabel.text = "Health"
        healthLabel.position = CGPoint(x: 585, y: 115)
        camera.addChild(healthLabel)
        
        paint.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        paint.position = CGPoint(x: 665, y: 200)
        camera.addChild(paint)
        
        paintLabel.fontColor = .white
        paintLabel.fontSize = 16
        paintLabel.text = "Paint"
        paintLabel.position = CGPoint(x: 665, y: 115)
        camera.addChild(paintLabel)
        
    }
    
    // Update Player 2 Section 1
    
    private func UPp2Sec1(Player: Player) {
        
        p2h.position.y -= (p2h.size.height - CGFloat((Player.health / 100.0) * 125.0)) / 2
        p2h.size.height = CGFloat((Player.health / 100.0) * 125.0)
        
        p2p.position.y -= (p2p.size.height - CGFloat((Player.paint / 100.0) * 125.0)) / 2
        p2p.size.height = CGFloat((Player.paint / 100.0) * 125.0)
        
    }
    
    // Player 1 Section 2
    
    private func p1GUISec2() {
        
        let ammo = p1a
        let ammoLabel = p1al
        
        let radius = p1r
        let radiusLabel = p1rl
        
        let speed = p1s
        let speedLabel = p1sl
        
        ammo.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        ammo.position = CGPoint(x: -665, y: 50)
        camera.addChild(ammo)
        
        ammoLabel.fontColor = .white
        ammoLabel.fontSize = 32
        ammoLabel.text = "1"
        ammoLabel.position = CGPoint(x: -585, y: 40)
        camera.addChild(ammoLabel)
    
        radius.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        radius.position = CGPoint(x: -665, y: -30)
        camera.addChild(radius)
        
        radiusLabel.fontColor = .white
        radiusLabel.fontSize = 32
        radiusLabel.text = "1"
        radiusLabel.position = CGPoint(x: -585, y: -40)
        camera.addChild(radiusLabel)
        
        speed.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        speed.position = CGPoint(x: -665, y: -110    )
        camera.addChild(speed)
        
        speedLabel.fontColor = .white
        speedLabel.fontSize = 32
        speedLabel.text = "1"
        speedLabel.position = CGPoint(x: -585, y: -120)
        camera.addChild(speedLabel)
        
    }

    // Update Player 1 Section 2
    
    private func UPp1Sec2(Player: Player) {
        
        p1sl.text = String(Player.moveLvL)
        p1rl.text = String(Player.radiusLvL)
        p1al.text = String(Player.ammoLvL)
        
    }
    
    // Player 2 Section 2
    
    private func p2GUISec2() {
        
        let ammo = p2a
        let ammoLabel = p2al
        
        let radius = p2r
        let radiusLabel = p2rl
        
        let speed = p2s
        let speedLabel = p2sl
        
        ammo.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        ammo.position = CGPoint(x: 585, y: 50)
        camera.addChild(ammo)
        
        ammoLabel.fontColor = .white
        ammoLabel.fontSize = 32
        ammoLabel.text = "1"
        ammoLabel.position = CGPoint(x: 665, y: 40)
        camera.addChild(ammoLabel)
        
        radius.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        radius.position = CGPoint(x: 585, y: -30)
        camera.addChild(radius)
        
        radiusLabel.fontColor = .white
        radiusLabel.fontSize = 32
        radiusLabel.text = "1"
        radiusLabel.position = CGPoint(x: 665, y: -40)
        camera.addChild(radiusLabel)
        
        speed.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        speed.position = CGPoint(x: 585, y: -110    )
        camera.addChild(speed)
        
        speedLabel.fontColor = .white
        speedLabel.fontSize = 32
        speedLabel.text = "1"
        speedLabel.position = CGPoint(x: 665, y: -120)
        camera.addChild(speedLabel)
        
    }
    
    // Update Player 2 Section 2
    
    private func UPp2Sec2(Player: Player) {
        
        p2sl.text = String(Player.moveLvL)
        p2rl.text = String(Player.radiusLvL)
        p2al.text = String(Player.ammoLvL)
        
    }
    
    // Player 1 Section 3
    
    private func p1GUISec3() {

        let killLabel = p1kl
        let deathLabel = p1dl
        let paintedLabel = p1ptl
        
        killLabel.fontColor = .white
        killLabel.fontSize = 24
        killLabel.text = "Kills: 0"
        killLabel.position = CGPoint(x: -625, y: -180)
        camera.addChild(killLabel)

        deathLabel.fontColor = .white
        deathLabel.fontSize = 24
        deathLabel.text = "Deaths: 0"
        deathLabel.position = CGPoint(x: -625, y: -220)
        camera.addChild(deathLabel)
        
        paintedLabel.fontColor = .white
        paintedLabel.fontSize = 24
        paintedLabel.text = "Painted: 0"
        paintedLabel.position = CGPoint(x: -625, y: -260)
        //camera.addChild(paintedLabel)
        
    }
    
    // Update Player 1 Section 3
    
    private func UPp1Sec3(Player: Player) {
        
        p1kl.text = "Kills: " + String(Player.kills)
        p1dl.text = "Deaths: " + String(Player.deaths)
        p1ptl.text = "Painted: " + String(Player.painted)
        
    }
    
    // Player 2 Section 3
    
    private func p2GUISec3() {
        
        let killLabel = p2kl
        let deathLabel = p2dl
        let paintedLabel = p2ptl
        
        killLabel.fontColor = .white
        killLabel.fontSize = 24
        killLabel.text = "Kills: 0"
        killLabel.position = CGPoint(x: 625, y: -180)
        camera.addChild(killLabel)
        
        deathLabel.fontColor = .white
        deathLabel.fontSize = 24
        deathLabel.text = "Deaths: 0"
        deathLabel.position = CGPoint(x: 625, y: -220)
        camera.addChild(deathLabel)
        
        paintedLabel.fontColor = .white
        paintedLabel.fontSize = 24
        paintedLabel.text = "Painted: 0"
        paintedLabel.position = CGPoint(x: 625, y: -260)
        //camera.addChild(paintedLabel)
        
    }
    
    // Update Player 2 Section 3
    
    private func UPp2Sec3(Player: Player) {
        
        p2kl.text = "Kills: " + String(Player.kills)
        p2dl.text = "Deaths: " + String(Player.deaths)
        p2ptl.text = "Painted: " + String(Player.painted)
        
    }
    
    public func createKillFeedObject(Player: String, Killed: String) {
        
        var yoffset:CGFloat = -450.0
        
        let killFeed:SKLabelNode = SKLabelNode(fontNamed: "Copperplate")
        killFeed.name = "KillFeed"
        killFeed.fontColor = .white
        killFeed.fontSize = 24
        killFeed.text = Player + " Killed " + Killed
        killFeed.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        
        for child in (self.camera?.children)! {
            if child.name == "KillFeed" {
                if child.position.y > yoffset {
                    
                    yoffset = child.position.y
                    
                }
            }
        }
        
        killFeed.position = CGPoint(x: 710, y: yoffset + (killFeed.frame.height + 10))
        self.camera?.addChild(killFeed)
        
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.removeKillFeed), userInfo: ["feed" : killFeed], repeats: false)
        
    }
    
    @objc func removeKillFeed(timer: Timer) {
        
        let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
        let killFeed:SKLabelNode = (userInfo["feed"] as! SKLabelNode)
        
        killFeed.removeFromParent()
        
    }
    
    @objc func timeTick(timer: Timer) {
        
        if (gameTime > 0) {
            gameTime -= 1
            timeLabel.text = String(gameTime)
        }
        else {
            timer.invalidate()
        }
        
    }
    
}

