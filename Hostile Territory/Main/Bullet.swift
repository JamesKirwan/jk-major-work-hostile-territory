//
//  Bullet.swift
//  Hostile Territory
//
//  Created by James Kirwan on 31/7/18.
//

import Foundation
import SpriteKit

class Bullet:SKSpriteNode {
    
    private let playerCategory:UInt32 = 0x1 << 0
    private let boxCategory:UInt32 = 0x1 << 1
    private let bulletCategory:UInt32 = 0x1 << 2
    private let pUpCategory:UInt32 = 0x1 << 3
    
    let player:Player!
    
    init(Player: Player) {
        
        player = Player
        
        var bulletText = SKTexture(imageNamed: "bulBlue")
        
        if (Player.playerColor == "Red") {
            
            bulletText = SKTexture(imageNamed: "bulRed")
            
        }
        
        super.init(texture: bulletText, color: .clear, size: bulletText.size())
        
        name = "Bullet"
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        zPosition = CGFloat(3.0)
        
        physicsBody = SKPhysicsBody(rectangleOf: size)
        physicsBody?.affectedByGravity = false
        
        physicsBody?.categoryBitMask = bulletCategory
        physicsBody?.contactTestBitMask = playerCategory | boxCategory | pUpCategory
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
